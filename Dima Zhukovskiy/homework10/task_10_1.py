class Flower:

    def __init__(self, name, days, color, length, price):
        self.price = price
        self.length = length
        self.color = color
        self.days = days
        self.name = name


class Bouquet:

    def __init__(self, *flower):
        self.full_bouquet = list(flower)

    def cost(self):
        cost_bouquet = []
        for flower in self.full_bouquet:
            cost_bouquet.append(flower.price)
        return sum(cost_bouquet)

    def withering_time(self):
        withering = []
        for flower in self.full_bouquet:
            withering.append(flower.days)
        return sum(withering) // len(withering)

    def sotr_color(self):
        flower_color = []
        for flower in self.full_bouquet:
            flower_color.append(flower.color)
        return sorted(flower_color)

    def sotr_length(self):
        flower_length = []
        for flower in self.full_bouquet:
            flower_length.append(flower.length)
        return sorted(flower_length)

    def sotr_price(self):
        flower_price = []
        for flower in self.full_bouquet:
            flower_price.append(flower.price)
        return sorted(flower_price)

    def sotr_days(self):
        flower_days = []
        for flower in self.full_bouquet:
            flower_days.append(flower.days)
        return sorted(flower_days)

    def search_flower(self, name, color):
        for flower in self.full_bouquet:
            if flower.name == name and flower.color == color:
                return f'{flower.color} {flower.name} есть в букете'
            else:
                return f'{flower.color} {flower.color} нет в букете'


rose_red = Flower('Роза', 10, 'красная', 5, 5)
rose_white = Flower('Роза', 8, 'белая', 8, 8)
rose_yellow = Flower('Роза', 5, 'желтая', 8, 10)
romashka_yellow = Flower('Ромашка', 13, 'желтая', 3, 4)


all_bouquet = Bouquet(rose_red, rose_white, romashka_yellow)
print(f'Букет стоит: {all_bouquet.cost()} рублей')
print(f'Он завянет через: {all_bouquet.withering_time()} дней')
print(f'сортировка букета по цвету {all_bouquet.sotr_color()}')
print(f'сортировка букета по длмне стебля {all_bouquet.sotr_length()}')
print(f'сортировка букета по цене {all_bouquet.sotr_price()}')
print(f'сортировка букета по свежести {all_bouquet.sotr_days()}')
# print(all_bouquet.search_flower(romashka_yellow))
# Не получается вывести цвет
