def decorator_order(func):
    def wrapper(*args):
        number_names = {0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four',
                        5: 'five', 6: 'six', 7: 'seven', 8: 'eight', 9: 'nine',
                        10: 'ten', 11: 'eleven', 12: 'twelve', 13: 'thirteen',
                        14: 'fourteen', 15: 'fifteen', 16: 'sixteen',
                        17: 'seventeen', 18: 'eighteen', 19: 'nineteen'}
        lex_args = []
        # заменяем числа на лексикографическое значение
        for i in args:
            for k, v in number_names.items():
                if i == k:
                    lex_args.append(v)
        # сортируем полученный лексикографический список
        ordered_array = sorted(lex_args)
        print_array = []
        # заменяем лексикографические значения на числа
        for i in ordered_array:
            for k, v in number_names.items():
                if i == v:
                    print_array.append(k)
        func(print_array)
    return wrapper


@decorator_order
def lex_order(*args):
    print(*args)


lex_order(1, 2, 3)
