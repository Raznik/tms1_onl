import wikipediaapi


def test_wiki_api():
    word = "programming language"
    en_wiki_page = wikipediaapi.Wikipedia('en')
    page_en = en_wiki_page.page(word)
    assert page_en.exists(), f"{word} doesn't exist"
    assert page_en.title == word, f"{page_en.title} is not equal {word}"
    print(f"Page title is {page_en.title}")
    print(f"Page body:\n {page_en.text}")
