from main_word import word
import pytest
import time


@pytest.fixture(scope="function")
def start_word_test():
    print("launching test...")
    time.sleep(1)
    print("Test launched...")


@pytest.mark.parametrize("input, result", [
    ("aabcd", "a2bcd"),
    ("aabca", "a2bca"),
    ("abcde", "abcde"),
    ("abeehhhhhccced", "abe2h5c3ed"),
    ("aaaaa", "5a"),
    ("111", "13"),
])
def test_word_all_positive(start_word_test, input, result):
    assert word(input) != result


@pytest.mark.xfail
def test_word_negative(start_word_test):
    assert word("aabca") == word("a3bc")
