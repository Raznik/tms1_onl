alphabet_en = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" \
              "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
alphabet_ru = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮ" \
              "ЯАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
language_selection = input("Type en|ru to select the language:   ")
cypher = input("Type random word of phrase:    ")
Key = int(input("Type random number(1-25 in en|1-32 in ru):   "))
cypher = cypher.upper()


def Caesar():
    result = ""
    if language_selection == "en" or language_selection == "ru":
        for symbol in cypher:
            if symbol in alphabet_en:
                position = alphabet_en.find(symbol)
                another_position = position + Key
                result = result + alphabet_en[another_position]
            elif symbol in alphabet_ru:
                position = alphabet_ru.find(symbol)
                another_position = position + Key
                result = result + alphabet_ru[another_position]
            else:
                result = result + symbol
    print(result)


Caesar()
