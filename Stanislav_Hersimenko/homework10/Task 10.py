import datetime


class Flower:
    def __init__(self, flowerName, price, length,
                 colour, freshness, creationTime=None):
        self.flowerName = flowerName
        self.price = price
        self.length = length
        self.colour = colour
        self.freshness = freshness
        self.flowerCreationTime = creationTime

    def print_flower(self):
        print(f"Цветок: {self.flowerName}, Цена: {self.price} BYN,\n"
              f"Длинна: {self.length} см, Цвет: {self.colour},\n"
              f"Cвежесть: {self.freshness}")

    def __repr__(self):
        return repr((self.flowerName, self.price, self.colour))


class Bouquet:
    def __init__(self, Bouquet_number):
        self.Bouquet_number = Bouquet_number
        self.flowers = []

    def add_flower(self, flower):
        self.flowers.append(flower)

    def print_flowers(self):
        result = 0
        life_time = self.lifetime_count()
        print("Букет номер", self.Bouquet_number, "собран из цветов:")
        for i in self.flowers:
            print(i.flowerName, "по цене", i.price, "BYN")
            result = result + i.price
        print("На сумму", result, "BYN")
        print("Среднее время жизни цветов", int(life_time / 60),
              "час(а,ов)", int(life_time % 60), "минут(а,ы)")

    def lifetime_count(self):
        cur_moment = datetime.datetime.now()
        common_lifetime = 0
        for i in self.flowers:
            common_lifetime += (cur_moment - i.flowerCreationTime).seconds
        return common_lifetime / len(self.flowers) / 60

    def search(self, parameter, value):
        search = {'имя': lambda:
                  [x for x in self.flowers if x.flowerName == value],
                  'цена': lambda:
                  [x for x in self.flowers if x.price == value],
                  'цвет': lambda:
                  [x for x in self.flowers if x.colour == value],
                  'длина': lambda:
                  [x for x in self.flowers if x.length == value],
                  'cвежесть': lambda:
                  [x for x in self.flowers if x.freshness == value]}
        if search.get(parameter):
            res = search[parameter]()
            for obj in res:
                print('\n')
                obj.print_flower()
        else:
            print("НИЧЕГО НЕ НАЙДЕНО")


# Цветы

Rose = Flower('Роза', 8, 30, 'Красный', 'Свежая',
              datetime.datetime(2021, 7, 13, 20, 21))
Lily = Flower('Лилия', 5, 15, 'Желтый', 'Свежая',
              datetime.datetime(2021, 7, 13, 22, 21))
Mayweed = Flower('Ромашка', 5, 10, 'Белый', 'Свежая',
                 datetime.datetime(2021, 7, 13, 22, 20))

# Формирование букета
bouquet = Bouquet(1)
bouquet.add_flower(Rose)
bouquet.add_flower(Lily)
bouquet.add_flower(Mayweed)

# Сортировка
# bouquet.sort_flowers('цвет')
bouquet.print_flowers()

bouquet.search('цена', 5)
bouquet.search('цвет', "Красный")

flower_sort = [Flower('Роза', 8, 30, 'Красный', 'Свежая'),
               Flower('Лилия', 5, 15, 'Желтый', 'Свежая'),
               Flower('Ромашка', 5, 10, 'Белый', 'Свежая')]

a = sorted(flower_sort, key=lambda flower: flower.price)
b = sorted(flower_sort, key=lambda flower: flower.colour)
c = sorted(flower_sort, key=lambda flower: flower.flowerName)
d = sorted(flower_sort, key=lambda flower: flower.length)
print('\n')
print(a)
print(b)
print(c)
print(d)
