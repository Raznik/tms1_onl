from collections import Counter
from string import punctuation


def give_me_your_str(your_str):
    # новая строка без лищних символов и пробелов
    new_str = ""
    for letter in your_str:
        # проверка на пробел, знак и цифру
        if letter != " " \
                and letter not in punctuation \
                and letter.isdigit() is not True:
            # буквы перевожу в нижний регистр, чтобы решить проблему A==a
            new_str += letter.lower()

    new_list = list(new_str)
    count_dict = Counter(new_list)
    # добавляю новый список, который хранит все самые частые буквы
    list_of_frequent_letters = []
    for k in count_dict.keys():
        if count_dict[k] == max(count_dict.values()):
            list_of_frequent_letters.append(k)
    # сортирую список, чтобы буквы стали по алфавиту
    list_of_frequent_letters = sorted(list_of_frequent_letters)
    print(list_of_frequent_letters[0])


give_me_your_str("a-z")
give_me_your_str("Hello World!")
give_me_your_str("How do you do?")
give_me_your_str("One")
give_me_your_str("Oops!")
give_me_your_str("AAaooo!!!!")
give_me_your_str("a" * 9000 + "b" * 1000)
give_me_your_str("199GGwp333ggWP!!!!")
