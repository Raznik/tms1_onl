from langdetect import detect

likes = ["William", "Michael", "Liam", "Alexander"]
lang = detect(' '.join(likes))
numbers = len(likes)
print(lang)

if lang == "ru":
    if numbers == 0:
        print("нет отметок")
    elif numbers == 1:
        print(likes[0], "нравится это")
    elif numbers == 2:
        print(likes[0], "и", likes[1], "нравиться это")
    elif numbers == 3:
        print(f"{likes[0]}, {likes[1]}", "и", likes[2], "нравится это")
    else:
        numbers >= 4
        print(f"{likes[0]}, {likes[1]}",
              "и", len(likes[2:]), "другим нравится это")
elif lang == "en":
    if numbers == 0:
        print("no one likes this")
    elif numbers == 1:
        print(likes[0], "likes this")
    elif numbers == 2:
        print(likes[0], "and", likes[1], "like this")
    elif numbers == 3:
        print(f"{likes[0]}, {likes[1]}", "and", likes[2], "like this")
    else:
        numbers >= 4
        print(f"{likes[0]}, {likes[1]}",
              "and", len(likes[2:]), "others like this")
