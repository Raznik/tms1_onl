list_of_names = []
list_of_names_1 = ["Ann"]
list_of_names_2 = ["Ann", "Alex"]
list_of_names_3 = ["Ann", "Alex", "Mark"]
list_of_names_4 = ["Ann", "Alex", "Mark", "Max"]


def fb_likes(people):
    if len(people) == 0:
        print("No one likes this")
    elif len(people) == 1:
        print(f"{people[0]} likes this")
    elif len(people) == 2:
        print(f"{people[0]} and {people[1]} likes this")
    elif len(people) == 3:
        print(f"{people[0]}, {people[1]} and {people[2]} like this")
    elif len(people) >= 4:
        print(f"{people[1]}, {people[2]} and {len(people[2::])}"
              f" others like this")


fb_likes(list_of_names_1)
fb_likes(list_of_names_2)
fb_likes(list_of_names_3)
fb_likes(list_of_names_4)
