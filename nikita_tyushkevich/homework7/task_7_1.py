# Tiered list

def reshape(list_numbers, row: int, items: int):
    # Check for number of elements in list
    if len(list_numbers) % row * items != 0:
        print('Incorrect numbers quantity')
    else:
        # Empty list for segments adding
        tiered_list = []
        items_index_number = items
        start_item_index = 0
        for i in range(0, row):
            # Getting slice from list as a segment for tiered_list
            list_slice = list_numbers[start_item_index:items_index_number]
            # Change start index for next segment
            start_item_index += items
            # Change end segment index
            items_index_number += items
            # Append list with gotten segment
            tiered_list.append(list_slice)
        print(tiered_list)


reshape([1, 2, 3, 4, 5, 6], 2, 3)
reshape([1, 2, 3, 4, 5, 6, 7, 8], 4, 2)
reshape([4, 5, 7, 2, 13, 54, 67, 11, 3, 99], 5, 2)
